const path = require("path");

function compile(code, file) {
	let ext = path.extname(file);

	if(ext == ".js") {
		return require("babel-core").transform(code, {
			plugins: [
				["transform-builtin-extend", {
					globals: ["Error", "Array"],
					approximate: true
				}],
				"transform-class-properties",
				"syntax-async-functions",
				"syntax-async-generators",
				"transform-async-to-generator",
				"transform-es2015-modules-commonjs",
				"transform-es2015-classes",
				"transform-es2015-block-scoping",
				"transform-es2015-for-of",
				"transform-es2015-shorthand-properties",
				"transform-es2015-arrow-functions"
			]
		}).code;
	} else if(ext == ".html") {
		return `
			var code = ${JSON.stringify(code)};
			var node = document.createElement("div");
			node.innerHTML = code;
			if(node.childNodes.length == 1) {
				// Return node itself
				module.exports = node.childNodes[0];
			} else {
				// Wrap in DocumentFragment
				var fragment = document.createDocumentFragment();

				var child;
				while(child = node.firstChild) {
					fragment.appendChild(child);
				}

				module.exports = fragment;
			}
		`;
	} else if(ext == ".css") {
		return `
			var code = ${JSON.stringify(code)};

			var node = document.createElement("style");
			node.type = "text/css";
			if(node.styleSheet) {
				node.styleSheet.cssText = code;
			} else {
				node.appendChild(document.createTextNode(code));
			}

			module.exports = node;
		`;
	} else {
		throw new Error("Unknown file extension " + ext);
	}
}

module.exports = compile;