const fs = require("fs-extra");
const path = require("path");
const glob = require("glob");


console.log("Gathering usermode");

console.log("---");
require("../usermode/build");
console.log("---");

console.log("Cleaning up");
fs.removeSync("./kernel/bin");

console.log("Copying");
fs.copySync("./usermode/dist", "./kernel/bin");