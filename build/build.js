const fs = require("fs");
const path = require("path");

// pushd
let oldPath = process.cwd();
process.chdir(path.resolve(__dirname, ".."));

require("./gather");

// Build loader
let code = "var __rc = {}; var __rf = {};\n";
code += "var require = function(path) {\n";
code += "    var resolve = function(relative, root) {\n";
code += "        if(relative[0] == '/') {\n";
code += "            return '.' + relative;\n";
code += "        }\n";

code += "        root = root.split('/').slice(0, -1).join('/');\n";
code += "        relative = root + '/' + relative;\n";
code += "        relative = relative.replace(/\\/+/, '/');\n";

code += "        var absolute = [];\n";
code += "        relative.split('/').forEach(function(part) {\n";
code += "            if(part == '.') {\n";
code += "                return;\n";
code += "            } else if(part == '..') {\n";
code += "                absolute.pop();\n";
code += "            } else {\n";
code += "                absolute.push(part);\n"
code += "            }\n";
code += "        });\n";
code += "        return './' + absolute.join('/');\n";
code += "    };\n";

code += "    let method = false;\n";
code += "    if(path.endsWith(':method')) {\n";
code += "        method = true;\n";
code += "        path = path.replace(/:method$/, '');\n";
code += "    }\n";

code += "    path = resolve(path, this.path);\n";

code += "    if(__rf.hasOwnProperty(path + '.js')) {\n";
code += "        path += '.js';\n";
code += "    } else if(__rf.hasOwnProperty(path + '.html')) {\n";
code += "        path += '.html';\n";
code += "    } else if(__rf.hasOwnProperty(path + '.css')) {\n";
code += "        path += '.css';\n";
code += "    } else if(__rf[path] && __rf[path].link) {\n";
code += "        path = __rf[path].link;\n";
code += "    }\n";

code += "    if(!__rf.hasOwnProperty(path)) {\n";
code += "        throw new Error('Unknown module being imported: ' + path)\n";
code += "    }\n";

code += "    if(method) {\n";
code += "        return __rf[path];\n";
code += "    }\n";

code += "    if(__rc.hasOwnProperty(path)) {\n";
code += "        return __rc[path];\n";
code += "    } else {\n";
code += "        var module = {root: false, path: path, exports: {}, drive: this.drive, bios: this.bios};\n";
code += "        __rf[path](module, module.exports, require.bind(module));\n";
code += "        return __rc[path] = module.exports;\n";
code += "    }\n";
code += "};\n";


// Incremental build
let originalBoot = "";
if(process.argv[2] == "incremental") {
	try {
		originalBoot = fs.readFileSync("./kernel/boot/__boot__.js", {
			encoding: "utf-8"
		});
	} catch(e) {
	}
}

console.log("Looking up boot files");
code += require("./pack")("./kernel/boot/**/*", "./kernel/boot/__boot__.js", originalBoot);

code += "var module = {root: true, path: './kernel/boot/loader.js', exports: {}, drive: drive, bios: bios};\n";
code += "__rf['./kernel/boot/loader.js'](module, module.exports, require.bind(module));";

console.log("Saving boot files");
fs.writeFileSync("./kernel/boot/__boot__.js", code);


// Incremental build
let originalKernel = "";
if(process.argv[2] == "incremental") {
	try {
		originalKernel = fs.readFileSync("./kernel/kernel/__kernel__.js", {
			encoding: "utf-8"
		});
	} catch(e) {
	}
}

console.log("Looking up kernel files");
let kernelCode = require("./pack")("./kernel/kernel/**/*", "./kernel/kernel/__kernel__.js", originalKernel);

console.log("Saving kernel files");
fs.writeFileSync("./kernel/kernel/__kernel__.js", kernelCode);


let kernel = path.resolve("./kernel");

console.log("Parsing rules");
let rules = path.resolve("./kernel/rules.txt");
rules = fs.readFileSync(rules, {
	encoding: "utf-8"
});

console.log("Encoding to image");
let buf = require("drv_node/encode")(kernel, rules);
fs.writeFileSync("kernel.drv", buf);


process.chdir(oldPath);