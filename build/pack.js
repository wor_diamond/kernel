const fs = require("fs");
const path = require("path");
const glob = require("glob");
const crypto = require("crypto");

function hash(data) {
	const hash = crypto.createHash("sha256");
	hash.update(data);
	return hash.digest("hex");
}

function pack(filter, ignore, original) {
	let code = "var _DIGEST_='';";

	let files = glob.sync(filter);
	files = files.filter(file => file != ignore);
	files.forEach(file => {
		console.log("Reading " + file);

		let contents;
		try {
			contents = fs.readFileSync(file, {
				encoding: "utf-8"
			});
		} catch(e) {
			if(e.code == "EISDIR") {
				return;
			}

			throw e;
		}


		let hashed = hash(contents);
		let before = "_DIGEST_=\"[[DIGEST_" + hashed + "_BEGIN]]\"";
		let after = "_DIGEST_=\"[[DIGEST_" + hashed + "_END]]\"";

		let compiled;
		if(original.indexOf(before) > -1) {
			console.log("Use compiled " + file);
			let part = original.substr(original.indexOf(before) + before.length + 1);
			compiled = part.substr(0, part.indexOf(after));
		} else {
			console.log("Compiling " + file);
			compiled = require("./compile.js")(contents, file);

			console.log("Validating " + file);
			try {
				new Function("", compiled);
			} catch(e) {
				if(e instanceof SyntaxError) {
					throw new SyntaxError("Invalid code: " + file + "\n" + e.message);
				} else {
					throw e;
				}
			}

			compiled = "__rf[" + JSON.stringify(file) + "] = function(module, exports, require) {" + compiled + "};\n";
		}

		code += before + ";" + compiled + after + ";";

		let woExt = path.basename(file, path.extname(file));
		if(woExt == "index") {
			// Also make it available as directory
			let dir = path.dirname(file);
			code += "__rf[" + JSON.stringify(dir) + "] = {link: " + JSON.stringify(file) + "};\n";
		}
	});

	console.log("Minifying");
	let result = require("uglify-es").minify(code, {
		warnings: true
	});
	if(result.warnings) {
		console.warn(result.warnings.join("\n"));
	} else if(result.error) {
		throw result.error;
	}
	return result.code;
}

module.exports = pack;