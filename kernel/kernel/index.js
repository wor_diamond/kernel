let node = module.bios.$(".bios");
node.parentNode.removeChild(node);

import TTY from "./tty";
import FileSystem from "../boot/fs";
import util from "../boot/util";
import Sandbox from "./sandbox";

(async function() {
	let fs = new FileSystem(module.drive);
	let userMode = await fs.readFile("bin/boot");
	if(userMode.type != "file") {
		(new TTY).write("/bin/boot is not a file");
		throw new Error("/bin/boot is not a file");
	}
	userMode = util.fromString(userMode.data);

	let sandbox = new Sandbox();
	sandbox.setProcess({
		path: "/bin/boot",
		argv: [],
		cwd: "",
		redirects: {
			0: new TTY,
			1: new TTY,
			2: new TTY
		}
	});
	sandbox.eval(userMode);
})();