import html from "./tty.html";
import css from "./tty.css";

class TTY {
	open(mode) {
		if(mode == "r") {
			window.onkeypress = e => {
				let chr = String.fromCharCode(e.charCode || e.keyCode || e.which || 0);
				if(chr == "\r") {
					chr = "\n";
				}

				TTY.buffer += chr;
				html.textContent += chr;
				if(chr == "\n" && TTY.readQueue.length) {
					TTY.readQueue.shift().resolve(TTY.buffer);
					TTY.buffer = "";
				}

				e.preventDefault();
				e.stopPropagation();
			};
		} else if(mode == "w") {
			document.body.appendChild(html);
			document.head.appendChild(css);
			return true;
		} else {
			throw new Error("Cannot open TTY from mode " + mode);
		}
	}

	wasOpened(mode) {
		// No need to open manually
		return mode == "r" || mode == "w";
	}

	write(data) {
		html.appendChild(document.createTextNode(data));
		html.scrollTop = html.scrollHeight;
	}

	async read() {
		if(TTY.buffer.indexOf("\n") > -1) {
			let part = TTY.buffer.substr(0, TTY.buffer.indexOf("\n") + 1);
			TTY.buffer = TTY.buffer.substr(TTY.buffer.indexOf("\n") + 1);
			return part;
		} else {
			return new Promise((resolve, reject) => {
				TTY.readQueue.push({resolve, reject});
			});
		}
	}
};

TTY.buffer = "";
TTY.readQueue = [];

export default TTY;