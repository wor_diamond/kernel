import TTY from "../tty";
import FileSystem from "../../boot/fs";

export default async function sandboxHandler(data) {
	// StdIO
	if(data.action == "stdio.open") {
		const id = data.id;
		const mode = data.mode;

		if(this.process.redirects[id] === null) {
			throw new Error("Stream #" + id + " was closed by remote side");
		}

		return await this.process.redirects[id].open(mode);
	} else if(data.action == "stdio.write") {
		const id = data.id;

		if(this.process.redirects[id] === null) {
			throw new Error("Stream #" + id + " was closed by remote side");
		} else if(!this.process.redirects[id].wasOpened("w")) {
			throw new Error("Stream #" + id + " was not opened for writing");
		}

		this.process.redirects[id].write(data.data);
	} else if(data.action == "stdio.read") {
		const id = data.id;

		if(this.process.redirects[id] === null) {
			throw new Error("Stream #" + id + " was closed by remote side");
		} else if(!this.process.redirects[id].wasOpened("r")) {
			throw new Error("Stream #" + id + " was not opened for reading");
		}

		return await this.process.redirects[id].read();
	}

	// Process
	if(data.action == "process.spawn") {
		const Sandbox = require(".").default;

		const path = data.path;
		const argv = data.argv;
		const redirects = Object.keys(data.redirects).map(from => {
			return [from, this.process.redirects[data.redirects[from]] || null];
		}).reduce((obj, val) => {
			obj[val[0]] = val[1];
			return obj;
		}, {});


		let fs = new FileSystem(module.drive);

		// Resolve relative path
		path = fs.resolve(path, this.process.cwd);

		let childProcess = await fs.readFile(path);
		if(childProcess.type != "file") {
			throw new Error(path + " is not a file");
		}
		childProcess = util.fromString(childProcess.data);

		let sandbox = new Sandbox();
		sandbox.setProcess({path, argv, redirects, cwd: this.process.cwd});
		sandbox.eval(childProcess);

		return await sandbox.whenExits();
	} else if(data.action == "process.exit") {
		if(!isNaN(Number(data.code))) {
			this.exitCode = Number(data.code);
		}
	}
}