onmessage = e => {
	if(e.data.action == "eval") {
		eval(e.data.code);
	} else if(e.data.action == "response") {
		postMessageWithResponse.awaiting[e.data.id].resolve(e.data.response);
	} else if(e.data.action == "responseError") {
		postMessageWithResponse.awaiting[e.data.id].reject(e.data.error);
	}
};

const stdio = new class StdIO {
	STDIN = 0;
	STDOUT = 1;
	STDERR = 2;

	constructor() {
		this.streams = {};
	}

	async open(id, mode) {
		if(this.streams[id]) {
			throw new Error("Stream #" + id + " was already opened for mode " + this.streams[id].mode);
		}

		if(mode == "w" || mode == "r") {
			this.streams[id] = {mode, opening: true};
			let res = await postMessageWithResponse({action: "stdio.open", mode, id});
			this.streams[id].opening = false;
			return res;
		} else {
			throw new Error("Unknown stream mode: " + mode)
		}
	}

	write(id, data) {
		if(!this.streams[id]) {
			throw new Error("Stream #" + id + " was not opened");
		} else if(this.streams[id].opening) {
			throw new Error("Stream #" + id + " is semiopened");
		} else if(this.streams[id].mode != "w") {
			throw new Error("Stream #" + id + " was not opened for writing");
		}

		postMessage({action: "stdio.write", data, id});
	}

	async read(id) {
		if(!this.streams[id]) {
			throw new Error("Stream #" + id + " was not opened");
		} else if(this.streams[id].opening) {
			throw new Error("Stream #" + id + " is semiopened");
		} else if(this.streams[id].mode != "r") {
			throw new Error("Stream #" + id + " was not opened for reading");
		}

		return await postMessageWithResponse({action: "stdio.read", id});
	}
};

const process = new class Process {
	async spawn(path, argv, redirects={}) {
		return await postMessageWithResponse({action: "process.spawn", path, argv, redirects});
	}

	exit(code) {
		postMessage({action: "process.exit", code: code});
		close();
	}
};

function postMessageWithResponse(message) {
	message.messageId = Math.random().toString(36).substr(2);

	postMessage(message);

	return new Promise((resolve, reject) => {
		postMessageWithResponse.awaiting[message.messageId] = {resolve, reject};
	});
}
postMessageWithResponse.awaiting = {};