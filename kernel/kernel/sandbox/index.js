import workerMethod from "./worker:method";
let workerCode = "";
workerCode += "var module = {exports: {}, root: false, worker: true, path: './kernel/kernel/sandbox/worker.js'};";
workerCode += "var require = function() { throw new Error('Cannot require() in Worker environment'); };";
workerCode += "(" + workerMethod.toString() + ")(module, module.exports, require);";

import sandboxHandler from "./handler";

export default class Sandbox {
	constructor() {
		this.worker = new Worker(URL.createObjectURL(new Blob([workerCode])));

		this.data = {
			process: null
		};
		this.exitCode = 0;
		this.worker.onmessage = async e => {
			try {
				const response = await sandboxHandler.call(this.data, e.data);
				if(e.data.messageId) {
					this._send({action: "response", id: e.data.messageId, response});
				}
			} catch(err) {
				console.error(err);
				if(e.data.messageId) {
					if(err instanceof Error) {
						err = {message: err.message};
					}

					this._send({action: "responseError", id: e.data.messageId, error: err});
				}
			}
		};

		this.exitPromises = [];
		this.exitted = false;
		this.worker.onclose = e => {
			this.exitted = true;
			this.exitPromises.forEach(promise => {
				promise.resolve(this.exitCode);
			});
		};
	}

	_send(data) {
		this.worker.postMessage(data);
	}

	eval(code) {
		this._send({action: "eval", code: code});
	}

	setProcess(process) {
		this.data.process = process;
	}

	async whenExits() {
		if(this.exitted) {
			return this.exitCode;
		}

		return new Promise((resolve, reject) => {
			this.exitPromises.push({resolve, reject});
		});
	}
};