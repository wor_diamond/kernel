import util from "util";

export default class FileSystem {
	constructor(drive) {
		this.drive = drive;
	}

	resolvePath(path) {
		let res = [];
		for(let part of path.split("/").filter(part => part != "")) {
			if(part == ".") {
				continue;
			} else if(part == "..") {
				res.pop();
			} else {
				res.push(part);
			}
		}
		return res.join("/");
	}
	resolve(path, root) {
		if(path[0] == "/") {
			return this.resolvePath(path);
		} else {
			return this.resolvePath(root + "/" + path);
		}
	}


	async readFile(path) {
		let header = await this.drive.readHeader();
		let rootFile = util.fromUint32(util.slice(header, 8, 4));

		let object = await this.readParseObject(rootFile);

		path = this.resolvePath(path).split("/");
		for await(let [i, part] of path.entries()) {
			if(object.type != "dir") {
				throw new Error(path.slice(0, i).join("/") + " is not a directory");
			}

			let file = object.data.find(obj => obj.name == part);
			if(!file) {
				throw new Error(path.slice(0, i + 1).join("/") + " does not exist");
			}

			object = await this.readParseObject(file.address);
		}

		return object;
	}

	async readParseObject(offset) {
		let originalOffset = util.formatAddress(offset);

		let data = [];

		let expectedType = null;
		let expectedPermissions = null;
		let expectedOwner = null;

		while(offset != 0) {
			let fragmentOffset = util.formatAddress(offset);

			// Read header
			let header = await this.drive.peek(offset, 44);

			// File type
			let type = header[0];
			if(expectedType === null) {
				expectedType = type;
			} else if(expectedType != type) {
				throw new Error("Inconsistency of type between fragments: expected " + expectedType + " (at " + originalOffset + "), got " + type + " (at " + fragmentOffset + ")");
			}

			// Permissions
			let permissions = {
				owner:    header[1],
				group:    header[2],
				everyone: header[3]
			};
			if(expectedPermissions === null) {
				expectedPermissions = permissions;
			} else if(permissions.owner != expectedPermissions.owner) {
				throw new Error("Inconsistency of owner permissions between fragments: expected " + expectedPermissions.owner + " (at " + originalOffset + "), got " + permissions.owner + " (at " + fragmentOffset + ")");
			} else if(permissions.group != expectedPermissions.group) {
				throw new Error("Inconsistency of group permissions between fragments: expected " + expectedPermissions.group + " (at " + originalOffset + "), got " + permissions.group + " (at " + fragmentOffset + ")");
			} else if(permissions.everyone != expectedPermissions.everyone) {
				throw new Error("Inconsistency of everyone permissions between fragments: expected " + expectedPermissions.everyone + " (at " + originalOffset + "), got " + permissions.everyone + " (at " + fragmentOffset + ")");
			}

			let fragmentSize = util.fromUint32(util.slice(header, 4, 4)); // Fragment size

			let nextFragment = util.fromUint32(util.slice(header, 8, 4)); // Next fragment

			// Owner
			let ownerBuffer = util.slice(header, 12, 32);
			let owner = [];
			for(let i = 0; i < ownerBuffer.length && ownerBuffer[i] != 0; i++) {
				owner.push(ownerBuffer[i]);
			}
			owner = util.fromString(owner);
			if(expectedOwner === null) {
				expectedOwner = owner;
			} else if(expectedOwner != owner) {
				throw new Error("Inconsistency of owner between fragments: expected " + expectedOwner + " (at " + originalOffset + "), got " + owner + " (at " + fragmentOffset + ")");
			}

			// Get data part
			let dataPart = await this.drive.peek(offset + 44, fragmentSize);
			data = data.concat(Array.from(dataPart));

			offset = nextFragment;
		}

		if(expectedType == 0) {
			return {type: "file", data: this.parseFile(data), permissions: expectedPermissions, owner: expectedOwner};
		} else if(expectedType == 1) {
			return {type: "dir", data: this.parseDirectory(data), permissions: expectedPermissions, owner: expectedOwner};
		}
	}
	parseFile(data) {
		return new Uint8Array(data);
	}
	parseDirectory(data) {
		let fileCount = util.fromUint32(util.slice(data, 0, 4)); // File count
		let localOffset = 4;

		let files = [];
		for(let i = 0; i < fileCount; i++) {
			// File name
			let name = [];
			while(data[localOffset] != 0) {
				name.push(data[localOffset]);
				localOffset++;
			}
			name = util.fromString(name);

			// Null terminator
			localOffset++;

			// Address
			let address = util.fromUint32(util.slice(data, localOffset, 4));
			localOffset += 4;

			files.push({name, address});
		}

		return files;
	}
};