import FileSystem from "fs";
import util from "util";

let bootFs = new FileSystem(module.drive);

(async function() {
	// Load pack
	let kernel = await bootFs.readFile("kernel/__kernel__.js");
	let code = util.fromString(kernel.data);
	eval(code);

	require("../kernel");
})();